# packer-vmware-alpine

Packer config for Alpine Linux VMs.

```bash
packer build \
    -var "esxi_host=ESXI_HOST_ADDRESS" \
    -var "esxi_datastore=ESXI_DS_NAME" \
    -var "esxi_username=ESXI_USER" \
    -var "esxi_password=ESXI_PASS" \
    -var "hostname=al-$(tr -dc A-Za-z0-9 </dev/urandom | head -c 10)" \
    alpine.json
```